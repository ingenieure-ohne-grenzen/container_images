<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ingenieure.cloud user administrator interface</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <link rel="shortcut icon" href="https://www.ingenieure-ohne-grenzen.org/favicon.ico" />

    <meta http-equiv="Refresh" content="0; url=${properties.FEATHER_FRONTEND_BASE_URL}" />
</head>
<body></body>
</html>
