function displayLogin(display){
    document.getElementById("kc-login").disabled = !display;
}

function updateMessages(messages) {
    let utcNow = new Date(Date.now()).getTime();
    let messageBoxParent = document.getElementById("kc-form");
    let messageBox = document.createElement("div");

    messageBoxParent.insertBefore(messageBox, messageBoxParent.firstChild);

    messages
      .filter((elem) => !elem.notAfter || new Date(elem.notAfter).getTime() > utcNow)
      .filter((elem) => !elem.notBefore || new Date(elem.notBefore).getTime() < utcNow)
      .forEach((elem) => {
        let node = document.createElement("div");
        node.classList.add("message");

        switch (elem.type.toLowerCase()) {
            case "warning":
                node.classList.add("warning");
                break;
            case "error":
                node.classList.add("error");
                break;
            default:
              node.classList.add("info");
              break;
          }
        let textNode = document.createTextNode(elem.message ? elem.message : "undefined");
        node.appendChild(textNode);
        messageBox.appendChild(node);
    });

    displayLogin(true);
}

document.addEventListener("DOMContentLoaded", () => {
    displayLogin(false);
    const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

    // If request lasts more then 2500 seconds, login button should be enabled again
    wait(2500).then(() => displayLogin(true))

    // Just log any error, login button should be enabled again
    fetch(document.getElementById("fetchURL").textContent)
        .then((data) => data.json())
        .then((data) => updateMessages(data))
        .catch((error) => {
            console.log(error);
            displayLogin(true);
        })
});

/* new Promise((resolve, reject) => {
    resolve([
      {
          type: "INFO",
          notBefore: null,
          notAfter: "2020-19-20T18:40:51.661Z",
          message: "Not any more visible"
      },
      {
          type: "INFO",
          notBefore: null,
          notAfter: "2020-12-22T18:40:51.661Z",
          message: "Visible, but will expire soon"
      },
      {
          type: "WARNING",
          notBefore: null,
          notAfter: null,
          message: "Always visible"
      },
      {
          type: "ERROR",
          notBefore: "2020-12-21T18:40:51.661Z",
          notAfter: null,
          message: "Visible in future"
      }
    ])
});*/
