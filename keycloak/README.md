# Keycloak

Our modified Keycloak image.

## Environment Variable Explanation

| VARIABLE                  | DEFAULT                                      | Description                                                                                                            |
| ------------------------- | -------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------- |
| KC_DB_URL_HOST            | postgresql.keycloak.svc                      | Sets the hostname of the default JDBC URL of the postgresql database                                                   |
| KC_DB_URL_DATABASE        | keycloak                                     | Sets the database name of the default JDBC URL of the postgresql database                                              |
| KC_DB_USERNAME            | keycloak                                     | The username of the database user                                                                                      |
| KC_DB_PASSWORD            | keycloak                                     | The password of the database user                                                                                      |
| KC_PROXY                  | edge                                         | The proxy address forwarding mode if the server is behind a reverse proxy. One of [edge, reencrypt, passthrough, none] |
| KC_HOSTNAME               | keycloak.apps.ingenieure.cloud               | Hostname for the Keycloak server                                                                                       |
| RESET_PASSWORD_LINK       | https://feather.apps.ingenieure.cloud/forgot | Where the user can reset his/her password                                                                              |
| MESSAGES_URL              | https://ingenieure.cloud/messages.json       | Pointing to a json file containing status messages. For an explanation of the schema please see below                  |
| FEATHER_API_BASE_URL      | https://feather-api.apps.ingenieure.cloud    | Where to find the user management tool api                                                                             |
| FEATHER_FRONTEND_BASE_URL | https://feather.apps.ingenieure.cloud        | Where to find the user management tool frontend                                                                        |
| FEATHER_ADMIN_ROLE        | admin                                        | Which role does identify a user as admin                                                                               |
| FEATHER_API_TOKEN         | feather-api-token                            | The token used to identify the plugin to the user management tool                                                      |

## Feather Plugin

On each user login, we want to check whether there are open actions in our user management tool [Feather](https://gitlab.com/ingenieure-ohne-grenzen/feather), for example the user needs to accept the gdpr. In this case the login is interrupted and the user is sent to Feather. The [plugin](https://gitlab.com/ingenieure-ohne-grenzen/keycloak-plugin) is installed as part of the build part from the `Dockerfile`.

## Database configuration

We use a [PostgreSQL](https://www.postgresql.org/) database to store the data. In the build part of the `Dockerfile` this is set. The connection details were provided via environment variables at runtime.

## Welcome Theme

If a user tries to directly access the Keycloak instance for whatever reason, he or she will be redirected to Feather.

## Login Theme

To display our logo, status messages and a proper password reset link, we changed the login theme a bit. Status messages are loaded lazily each time a user tries to login. Initially the login button is disabled until the messages are loaded. It will also be reactivated after 2.5 seconds to enable login if the status messages are unreachable. The following example displays all possible configuration values. **Note:** If no timezone offset is given, the local time zone of the user is used, else specifiy the date string in ISO-8601. Notice the summer and winter time changes!

```json
[
  {
    "type": "INFO",
    "notBefore": null,
    "notAfter": null,
    "message": "This message is displayed always in green color"
  },
  {
    "type": "WARNING",
    "notBefore": "2020-02-25T21:45:00+01:00",
    "notAfter": null,
    "message": "This message is displayed in orange color after the specified date and time in winter term. It will then be displayed forever."
  },
  {
    "type": "ERROR",
    "notBefore": null,
    "notAfter": "2020-06-26T00:00:00+02:00",
    "message": "This message is displayed in red color until the specified date and time in summer term. It will then disappear. Notice: You can also specify both of notBefore and notAfter"
  }
]
```

## Other Configuration

Currently health check and metrics are disabled. This is subject to change.
