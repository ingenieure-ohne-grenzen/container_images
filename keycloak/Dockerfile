ARG KEYCLOAK_VERSION=26.0.6

FROM redhat/ubi8-minimal:8.10-1052 as pre-builder
ARG KEYCLOAK_VERSION

# renovate: datasource=gitlab-packages depName=ingenieure-ohne-grenzen/keycloak-plugin:dev/maximilian/feather/keycloak/feather-keycloak-plugin versioning=semver registryUrl=https://gitlab.com
ENV FEATHER_PLUGIN_VERSION=1.0.37

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN microdnf install -y unzip && \
    mkdir -p /opt/keycloak/providers/ && \
    curl -sSL "https://gitlab.com/api/v4/groups/6691377/-/packages/maven/dev/maximilian/feather/keycloak/feather-keycloak-plugin/$FEATHER_PLUGIN_VERSION/feather-keycloak-plugin-$FEATHER_PLUGIN_VERSION-all.jar" -o /opt/keycloak/providers/feather-keycloak-plugin.jar && \
    TARGET_KEYCLOAK_VERSION=$(unzip -p /opt/keycloak/providers/feather-keycloak-plugin.jar META-INF/MANIFEST.MF | grep "Target-Keycloak-Version" | awk -F: '{ print $2 }' | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//') && \
    if [ "$TARGET_KEYCLOAK_VERSION" != "$KEYCLOAK_VERSION" ]; then echo "The keycloak version this image is currently build for matches not the keycloak version the plugin was built for. (Image Keycloak) $TARGET_KEYCLOAK_VERSION != $KEYCLOAK_VERSION (Plugin Keycloak)"; exit 1; fi && \
    microdnf clean all

# Adapted from https://www.keycloak.org/server/containers
FROM quay.io/keycloak/keycloak:$KEYCLOAK_VERSION as builder

ENV KC_HEALTH_ENABLED=false
ENV KC_METRICS_ENABLED=false
ENV KC_DB=postgres

# Needed for instantiation of FeatherPluginAuthFactory, overwritten on runtime
ENV FEATHER_API_BASE_URL=https://feather-api.apps.ingenieure.cloud
ENV FEATHER_FRONTEND_BASE_URL=https://feather.apps.ingenieure.cloud
ENV FEATHER_ADMIN_ROLE=admin

COPY iog-dkp-theme /opt/keycloak/themes/iog-dkp
COPY --from=pre-builder /opt/keycloak/providers/ /opt/keycloak/providers/

RUN /opt/keycloak/bin/kc.sh build && \
    mkdir /opt/keycloak/.keycloak && \
    chmod 770 /opt/keycloak/.keycloak

FROM quay.io/keycloak/keycloak:$KEYCLOAK_VERSION
COPY --from=builder /opt/keycloak/ /opt/keycloak/
WORKDIR /opt/keycloak

## These are not actual production values, just here to document what we set in production
## Docs here: https://www.keycloak.org/server/all-config?f=config

# Where to find the database
ENV KC_DB_URL_HOST=postgresql.keycloak.svc
ENV KC_DB_URL_DATABASE=keycloak
ENV KC_DB_USERNAME=keycloak
ENV KC_DB_PASSWORD=keycloak

# Where this instance can be found, behind a reverse proxy
ENV KC_PROXY=edge
ENV KC_HOSTNAME=keycloak.apps.ingenieure.cloud

# Our specific configuration for the reset password link and to display useful status information to users
ENV RESET_PASSWORD_LINK=https://feather.apps.ingenieure.cloud/forgot
ENV MESSAGES_URL=https://ingenieure.cloud/messages.json

# Configuration for the feather plugin
ENV FEATHER_API_BASE_URL=https://feather-api.apps.ingenieure.cloud
ENV FEATHER_FRONTEND_BASE_URL=https://feather.apps.ingenieure.cloud
ENV FEATHER_ADMIN_ROLE=admin
ENV FEATHER_API_TOKEN=feather-api-token

# Start normally with the specific welcome theme
ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start", "--spi-theme-welcome-theme=iog-dkp"]
