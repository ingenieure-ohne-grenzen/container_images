# https://hub.docker.com/_/drupal/tags?page=&page_size=&ordering=&name=php8.2-apache-bookworm
FROM drupal:10.3.10-php8.2-apache-bookworm
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
WORKDIR /usr/local/bin
# https://github.com/civicrm/civicrm-core/tags
ENV CIVICRM_VERSION=5.80.1
# https://lab.civicrm.org/extensions/civirules/-/releases
ENV CIVICRM_EXT_CIVIRULES_VERSION=3.14.0
# https://lab.civicrm.org/extensions/theisland/-/releases
ENV CIVICRM_EXT_THEISLAND_VERSION=2.4.2
# https://lab.civicrm.org/extensions/configitems/-/tags
ENV CIVICRM_EXT_CIVICONFIG_VERSION=2.0.0
# https://github.com/aptible/supercronic/releases
ENV SUPERCRONIC_VERSION=0.2.33
ENV SUPERCRONIC_SHA1SUM=71b0d58cc53f6bd72cf2f293e09e294b79c666d8

RUN set -eux; \
    \
    if command -v a2enmod; then \
        a2enmod rewrite; \
    fi; \
    \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        libfreetype6-dev=2.12* \
        libicu-dev=72* \
        libldap2-dev=2.5.13* \
        wget=1.21* \
        unzip=6.0-* \
        git=1:2* \
        libzip-dev=1.7* \
        libjpeg-dev=1:2.1* \
        libwebp-dev=1.2* \
        libpng-dev=1.6* \
        netcat-traditional=1.10* \
        default-mysql-client=1.1* \
    ; \
    pecl channel-update pecl.php.net; \
    pecl install apcu; \
    docker-php-ext-enable apcu; \
    docker-php-ext-configure gd \
            --with-freetype \
            --with-jpeg=/usr \
            --with-webp \
        ;\
    docker-php-ext-configure intl ;\
    docker-php-ext-configure ldap ;\
    docker-php-ext-install -j "$(nproc)" \
            bcmath \
            gd \
            intl \
            ldap \
            mysqli \
            opcache \
            pdo_mysql \
            zip \
        ; \
    \
    rm -rf /var/lib/apt/lists/* ; \
    curl -LsS https://download.civicrm.org/cv/cv.phar -o /usr/local/bin/cv; \
    chmod +x /usr/local/bin/cv;

# Add regular cron job
# Uses supercronic (https://www.aptible.com/blog/cron-for-containers-introduction-supercronic)
# Latest releases available at https://github.com/aptible/supercronic/releases
# Has to be started from a separate container using the command: /usr/local/bin/supercronic /tmp/civicrm.crontab
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v$SUPERCRONIC_VERSION/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64

# Upload local files to /tmp in the container:
COPY ./cron/civicrm.crontab ./cron/civicrm_cron.sh /tmp/

RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && chmod +x "/tmp/civicrm_cron.sh" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic;

WORKDIR /opt/drupal
RUN set -eux; \
    COMPOSER_HOME="$(mktemp -d)"; \
    export COMPOSER_ALLOW_SUPERUSER=1 ; \
    ln -sf /opt/drupal/web /var/www/html ; \
    composer config extra.enable-patching true ; \
    composer config minimum-stability dev ; \
    composer config --no-plugins allow-plugins.cweagans/composer-patches true ; \
    composer config --no-plugins allow-plugins.civicrm/composer-downloads-plugin true ; \
    composer config --no-plugins allow-plugins.civicrm/composer-compile-plugin true ; \
    composer config --no-plugins allow-plugins.civicrm/civicrm-asset-plugin true ; \
    composer config --no-plugins allow-plugins.mglaman/composer-drupal-lenient true;\
    composer config --no-plugins allow-plugins.szeidler/composer-patches-cli true;\
    composer config extra.compile-mode all ; \
    composer require civicrm/civicrm-asset-plugin ; \
    composer require civicrm/civicrm-core:${CIVICRM_VERSION}  --with-all-dependencies  ; \
    composer require civicrm/civicrm-packages:${CIVICRM_VERSION}  --with-all-dependencies ; \
    composer require civicrm/civicrm-drupal-8:${CIVICRM_VERSION} --with-all-dependencies ; \
    composer require drush/drush; \
    composer require 'drupal/ldap:^4.12'; \
    composer require 'drupal/simple_oauth:^5.2.5'; \
    composer require 'drupal/openid_connect:^1.4'; \
    composer require 'drupal/keycloak:^1.8'; \
    rm -rf "$COMPOSER_HOME" ; \
    curl -L https://download.civicrm.org/civicrm-$CIVICRM_VERSION-l10n.tar.gz --output civicrm-$CIVICRM_VERSION-l10n.tar.gz ; \
    tar -zxf civicrm-$CIVICRM_VERSION-l10n.tar.gz; \
    mkdir -p /opt/drupal/vendor/civicrm/civicrm-core/l10n/de_DE/LC_MESSAGES/; \
    cp civicrm/l10n/de_DE/LC_MESSAGES/civicrm.mo /opt/drupal/vendor/civicrm/civicrm-core/l10n/de_DE/LC_MESSAGES/civicrm.mo; \
    mkdir -p /opt/drupal/vendor/civicrm/civicrm-core/sql/; \
    cp civicrm/sql/civicrm_acl.de_DE.mysql /opt/drupal/vendor/civicrm/civicrm-core/sql/civicrm_acl.de_DE.mysql; \
    cp civicrm/sql/civicrm_data.de_DE.mysql /opt/drupal/vendor/civicrm/civicrm-core/sql/civicrm_data.de_DE.mysql; \
    mkdir -p /opt/drupal/vendor/civicrm/extensions/; \
    rm -rf civicrm; \
    rm civicrm-$CIVICRM_VERSION-l10n.tar.gz;

RUN set -eux; \
    curl -L https://lab.civicrm.org/extensions/civirules/-/archive/$CIVICRM_EXT_CIVIRULES_VERSION/civirules-$CIVICRM_EXT_CIVIRULES_VERSION.zip --output civirules-$CIVICRM_EXT_CIVIRULES_VERSION.zip; \
    unzip civirules-$CIVICRM_EXT_CIVIRULES_VERSION.zip; \
    mv civirules-$CIVICRM_EXT_CIVIRULES_VERSION /opt/drupal/vendor/civicrm/extensions/org.civicoop.civirules; \
    rm civirules-$CIVICRM_EXT_CIVIRULES_VERSION.zip;

RUN set -eux; \
    curl -L https://lab.civicrm.org/extensions/theisland/-/archive/$CIVICRM_EXT_THEISLAND_VERSION/theisland-$CIVICRM_EXT_THEISLAND_VERSION.zip --output theisland-$CIVICRM_EXT_THEISLAND_VERSION.zip; \
    unzip theisland-$CIVICRM_EXT_THEISLAND_VERSION.zip ; \
    mv theisland-$CIVICRM_EXT_THEISLAND_VERSION /opt/drupal/vendor/civicrm/extensions/theisland; \
    rm theisland-$CIVICRM_EXT_THEISLAND_VERSION.zip;

RUN set -eux; \
    curl -L https://lab.civicrm.org/extensions/configitems/-/archive/$CIVICRM_EXT_CIVICONFIG_VERSION/configitems-$CIVICRM_EXT_CIVICONFIG_VERSION.zip --output configitems-$CIVICRM_EXT_CIVICONFIG_VERSION.zip; \
    unzip configitems-$CIVICRM_EXT_CIVICONFIG_VERSION.zip; \
    mv configitems-$CIVICRM_EXT_CIVICONFIG_VERSION /opt/drupal/vendor/civicrm/extensions/org.civicoop.configitems; \
    sed -i 's/"which ".\(\$\w*\)/["which", \1]/g' /opt/drupal/vendor/civicrm/extensions/org.civicoop.configitems/Civi/ConfigItems/Commands/*.php; \
    rm configitems-$CIVICRM_EXT_CIVICONFIG_VERSION.zip;

COPY iog-group-permissions /opt/drupal/vendor/civicrm/extensions/iog-group-permissions
COPY iog_civicrm_css /opt/drupal/web/modules/custom/iog_civicrm_css

WORKDIR /opt
RUN set -eux; \
    tar -czf /usr/local/src/drupal.tar.gz drupal

COPY docker-entrypoint.sh /usr/local/bin/
COPY ingenieure /usr/local/src/ingenieure
WORKDIR /usr/local/bin
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
VOLUME [ "/opt" ]

RUN mkdir /etc/ldap; { \
    echo 'TLS_CACERT /etc/ssl/certs/ca-certificates.crt' ; \
    } > /etc/ldap/ldap.conf

RUN { \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=60'; \
        echo 'opcache.fast_shutdown=1'; \
    } > /usr/local/etc/php/conf.d/opcache-recommended.ini

RUN { \
        echo 'memory_limit=1600M'; \
        echo 'max_execution_time=240'; \
        echo 'max_input_time=120'; \
        echo 'post_max_size=50M'; \
        echo 'upload_max_filesize=50M'; \
    } > /usr/local/etc/php/conf.d/civicrm-settings.ini

RUN { \
    echo 'ServerName 127.0.0.1'; \
    } >> /etc/apache2/apache2.conf

RUN set -eux; \
    sed -i 's/Listen 80/Listen 8083/g' /etc/apache2/ports.conf; \
    sed -i 's/<VirtualHost *:80>/<VirtualHost *:8083>/g' /etc/apache2/sites-available/000-default.conf; \
    chmod a+r /usr/local/src/drupal.tar.gz; \
    touch /usr/local/src/drupal.tar.gz; \
    apt-get -y --allow-remove-essential --purge --autoremove remove libfreetype6-dev libicu-dev libldap2-dev libzip-dev libjpeg-dev libwebp-dev libpng-dev; \
    rm -rf /var/lib/apt/lists/*; \
    chmod +x /usr/local/bin/docker-entrypoint.sh; \
    # Remove /var/www/html so that on first run a new symlink /opt/drupal/web -> /var/www/html can be created \
    rm -R /var/www/html; \
    chmod a+rw /var/www;

ENTRYPOINT ["docker-entrypoint.sh"]
