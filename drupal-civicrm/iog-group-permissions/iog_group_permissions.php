<?php

define('IOGGROUP_PERMISSION_NAME', 'Erlaubt Kontakte zu Gruppen hinzuzufügen oder aus Gruppen zu löschen.');

require_once 'iog_group_permissions.civix.php';
// phpcs:disable
use CRM_IogGroupPermissions_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function iog_group_permissions_civicrm_config(&$config) {
  _iog_group_permissions_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function iog_group_permissions_civicrm_install() {
  _iog_group_permissions_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function iog_group_permissions_civicrm_enable() {
  _iog_group_permissions_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_permission().
 *
 * @see CRM_Utils_Hook::permission()
 * @param array $permissions
 *
 * Führt die neue CMS Berechtigung für Gruppenbearbeitungsrechte ein.
 *
 */
function iog_group_permissions_civicrm_permission(&$permissions) {
  $permissions[IOGGROUP_PERMISSION_NAME] = [
    'label' => E::ts('CiviCRM IoG Group Permissions') . ': ' . E::ts('Berechtigung für die Verwaltung von Kontaktgruppen'),
    'description' => E::ts('Erlaubt Kontakte zu Gruppen hinzuzufügen oder aus Gruppen zu löschen.'),
  ];
}

/**
 * Implements hook_civicrm_searchTasks();
 *
 * @param string $objectName
 * @param array $tasks
 *
 * Entfernt die Gruppenbearbeitungsaktionen aus dem Drup/Down Menü von Such-Ergebnislisten.
 *
 */
function iog_group_permissions_civicrm_searchTasks($objectName, &$tasks) {
  if (!CRM_Core_Permission::check(IOGGROUP_PERMISSION_NAME)) {
    unset($tasks[CRM_Core_Task::GROUP_ADD]);
    unset($tasks[CRM_Core_Task::GROUP_REMOVE]);
    unset($tasks[CRM_Core_Task::SAVE_SEARCH]);
  }
}

/**
 * Implements hook__civicrm_validateForm
 *
 * If the user is not permitted to add contact to a group the hook blocks adding a contact to a group via the
 * button 'hinzufügen' in 'Gruppen' of the contact summary form.
 *
 * Gibt eine Fehlermeldung aus wenn im Guppenfenster des Kontakt-Summary Formulars versucht wird den Kontakt zu
 * einer Gruppe hinzuzufügen.
 *
*/

function iog_group_permissions_civicrm_validateForm($formName, &$fields, &$files, &$form, &$errors) {

  if (!CRM_Core_Permission::check(IOGGROUP_PERMISSION_NAME)) {
    if ($formName == 'CRM_Contact_Form_GroupContact') {
      $errors['group_id'] = 'Keine Berechtigung um Kontakte zur Gruppe hinzuzufügen';
    }
  }
}

/**
 * Implements hook__civicrm_validateForm
 *
 * Entfernt in jeder Zeile einer Such-Ergebnisliste die Gruppenbearbeitungsaktion.
 * 
*/


function iog_group_permissions_civicrm_links($op, $objectName, $objectId, &$links, &$mask, &$values) {

 // var_dump($links);
  if (!CRM_Core_Permission::check(IOGGROUP_PERMISSION_NAME)) {

    switch ($objectName) {
      case 'Contact':
        switch ($op) {
          case 'contact.selector.actions':

            foreach ($links as $linkId => $linkValues) {
              if (isset($linkValues['ref']) && $linkValues['ref'] == 'group-add-contact') {          
                unset($links[$linkId]);
              }
            }
        }
     }
  }
}

/**
 * Implements hook_civicrm_buildForm().
 *
 * @param string $formName
 * @param CRM_Core_Form $form
 *
 * Erntfernt den Button zum hinzufügen von Kontakten aus dem Formular der Gruppen-Kontaktliste.
 *
 */

function iog_group_permissions_civicrm_buildForm($formName, &$form) {
 
  if (!CRM_Core_Permission::check(IOGGROUP_PERMISSION_NAME)) {

    if ($formName == 'CRM_Contact_Form_Search_Basic') {
      CRM_Core_Region::instance('page-body')->add(array('template' => 'RemoveAddToGroupButton.tpl'));
    }
  }
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 */
//function iog_group_permissions_civicrm_preProcess($formName, &$form) {
//
//}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
//function iog_group_permissions_civicrm_navigationMenu(&$menu) {
//  _iog_group_permissions_civix_insert_navigation_menu($menu, 'Mailings', [
//    'label' => E::ts('New subliminal message'),
//    'name' => 'mailing_subliminal_message',
//    'url' => 'civicrm/mailing/subliminal',
//    'permission' => 'access CiviMail',
//    'operator' => 'OR',
//    'separator' => 0,
//  ]);
//  _iog_group_permissions_civix_navigationMenu($menu);
//}
