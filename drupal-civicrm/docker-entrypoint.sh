#!/bin/bash
export HOME=/tmp

install_internal_db(){
    # this case is for testing and expects root access
    echo "Install internal database..."
    apt-get update
    apt-get install -y --no-install-recommends mariadb-server=1:10*
    rm -rf /var/lib/apt/lists/* 
    mysql_install_db
    echo "Start internal database..."
    service mariadb start
    mysqladmin -u root password $DB_PASSWORD
    mysqladmin -u root --password=$DB_PASSWORD create $DB_NAME
}

wait_for_db(){
    # this case is for production and may not use root permissions
    echo "Wait for database ..."
    while ! netcat -z -v $DB_HOST 3306; do
        sleep 1
        echo "Wait for mysql at $DB_HOST up"
    done
    echo "Database ready."
}

install_drupal(){
    echo The Hostname is: $HOSTNAME
    cat /etc/hosts
    wait_for_db
    cd /opt/drupal
    echo "Starting Drupal Installation"
    drush si ingenieure --db-url=mysql://$DB_USER:$DB_PASSWORD@$DB_HOST:3306/$DB_NAME --account-pass=$DRUPAL_ADMIN_PASSWORD --site-name=$SITE_NAME -y install_configure_form.enable_update_status_emails=NULL
    chmod a+w /opt/drupal/web/sites/default/settings.php
    # 127.0.0.1 is necessary for pipeline test
    # ^10. is necessary to accept health checks beginning with ip 10. in okd4
    printf "\$settings['trusted_host_patterns'] = [\n\t'%s$',\n\t'127.0.0.1',\n\t'^10.'\n];" $DRUPAL_TRUSTED_HOST >> /opt/drupal/web/sites/default/settings.php
    # restrict session life time to 6 hrs
    sed -i 's/gc_maxlifetime: 200000/gc_maxlifetime: 21600/g' /opt/drupal/web/sites/default/default.services.yml
    sed -i 's/cookie_lifetime: 2000000/cookie_lifetime: 21600/g' /opt/drupal/web/sites/default/default.services.yml

    chmod a-w /opt/drupal/web/sites/default/settings.php
    echo "Drupal Installation finished"
}

install_civicrm(){
    echo "Starting CiviCRM Installation"

    cv core:install --cms-base-url="https://$DRUPAL_TRUSTED_HOST" --lang="de_DE"
    chmod a-w /opt/drupal/web/sites/default/civicrm.settings.php

    echo "Finished CiviCRM Installation"
}

install_civicrm_ext(){
    echo "Starte Installation von CiviCRM Extensions"
    chmod a+w /opt/drupal/web/sites/default
    chmod a+w /opt/drupal/web/sites/default/civicrm.settings.php
    #Change CiviCRM extensionsDir to extensionsDir
    sed -i "s/ \\/\\/ \$civicrm_setting\\['domain'\\]\\['extensionsDir'\\] = '\\/path\\/to\\/extensions-dir';/\$civicrm_setting['domain']['extensionsDir'] = '\\/opt\\/drupal\\/vendor\\/civicrm\\/extensions';/g" /opt/drupal/web/sites/default/civicrm.settings.php
    rm -rf /opt/drupal/web/sites/default/files/civicrm/ext
    ln -s /opt/drupal/vendor/civicrm/extensions /opt/drupal/web/sites/default/files/civicrm/ext
    chmod a-w /opt/drupal/web/sites/default/civicrm.settings.php
    chmod a-w /opt/drupal/web/sites/default

    cv en authx civirules civiconfig theisland iog-group-permissions

    echo "Installation der CiviCRM-Extensions beendet"
}

install_civicrm_ext_config(){
    echo "Starte Konfiguration von CiviCRM Extensions"
    chmod a+w /opt/drupal/web/sites/default/civicrm.settings.php
	
    cv ev 'Civi::settings()->set("authx_guards", []);'
    cv ev 'Civi::settings()->set("authx_header_cred", ["jwt", "api_key"]);'
    call='{"values":{"contact_type":"Individual","display_name":"APIuser","first_name":"API","last_name":"User","api_key":"'$CIVICRM_APIKEY'"},"chain":{"name_me_0":["Email","create",{"values":{"contact_id":"$id","email":"api_user@localhost"}}]}}'
    echo $call | cv api4 Contact.create --in=json -q
    drush user:create APIUser --mail="api_user@localhost" --password="apiuser"
    drush role:create 'api' 'API'
    drush role:perm:add api 'authenticate with api key,access AJAX API,all CiviCRM permissions and ACLs'
    drush role:create 'rg-member' 'RG Member'
    drush role:perm:add rg-member 'access CiviCRM, access CiviReport, access Contact Dashboard, access Report Criteria, access contact reference fields, access uploaded files, add contact notes, add contacts, administer Reports, administer private reports, manage tags, profile listings, profile listings and forms, profile view, save Report Criteria, view all activities, view all notes, view my contact'
    #drush role:perm:add rg-member 'access CiviContribute, view contributions of type Spende, view contributions of type Zuwendung einer Stiftung'
    drush role:create 'ethikpruefer' 'Ethikpruefer'
    drush role:perm:add ethikpruefer 'access CiviCRM,access CiviReport, access Contact Dashboard, access Report Criteria, access contact reference fields, access uploaded files, add contact notes, administer Reports, administer private reports, manage tags, profile listings, profile listings and forms, profile view, save Report Criteria, view all activities, view all notes, view my contact'
    drush role:create 'geschaeftsstelle' 'Geschaeftsstelle'
    drush role:perm:add geschaeftsstelle 'access CiviCRM, access CiviReport, access Contact Dashboard, access Report Criteria, access all custom data, access contact reference fields, access deleted contacts, access uploaded files, add contact notes, add contacts, administer CiviCRM data, administer CiviCRM system, administer Reports, administer private reports, administer reserved reports, delete activities, delete contacts, manage tags, profile listings, profile listings and forms, profile view, save Report Criteria, view all activities, view all contacts, view all notes, view my contact, view report sql'
    #drush role:perm:add geschaeftsstelle 'access CiviContribute, add contributions of type Mitgliedsbeitrag, add contributions of type Spende, add contributions of type Zuwendung einer Stiftung, administer CiviCRM Financial Types, delete contributions of type Mitgliedsbeitrag, delete contributions of type Spende, delete contributions of type Zuwendung einer Stiftung, delete in CiviContribute, edit contributions, edit contributions of type Mitgliedsbeitrag, edit contributions of type Spende, edit contributions of type Zuwendung einer Stiftung, view contributions of type Mitgliedsbeitrag, view contributions of type Spende, view contributions of type Zuwendung einer Stiftung'
    drush user-add-role "api" APIUser
    drush en ldap_authorization, authorization, authorization_drupal_roles -y
    drush config:set civicrmtheme.settings admin_theme claro -y

    chmod a-w /opt/drupal/web/sites/default/civicrm.settings.php
    chmod -R a+rw /opt/drupal/web/sites/default/files/
    echo "Konfiguration der CiviCRM-Extensions beendet"
}

if [ ! -f "/opt/drupal/finished" ]; then
    echo "First Run!"
    echo "Extracting drupal.tar.gz"
    cd /opt
    tar --skip-old-files -xpzf /usr/local/src/drupal.tar.gz drupal

    ln -sf /opt/drupal/web /var/www/html
    mkdir -p /opt/drupal/web/profiles/ingenieure
    cp -R /usr/local/src/ingenieure /opt/drupal/web/profiles
    if [ "$EXTERNAL_DATABASE" == "1" ]; then
        echo "Use external database..."
    else 
        install_internal_db
    fi
 
    install_drupal
    install_civicrm
    install_civicrm_ext
    install_civicrm_ext_config

    touch /opt/drupal/finished
elif [[ /usr/local/src/drupal.tar.gz -nt /opt/drupal/finished ]]; then
    echo "Update required!"

    echo "Enable maintenance mode"
    drush maint:set 1

    echo "Backup /opt/drupal/web/sites directory to /tmp"
    mv /opt/drupal/web/sites /tmp/
    mv /opt/drupal/finished /tmp

    echo "Clearing /opt/drupal"
    rm -rf /opt/drupal/*
    # restore finished to prevent first run on errors
    mv /tmp/finished /opt/drupal/

    echo "Extracting drupal.tar.gz"
    cd /opt
    tar --skip-old-files -xpzf /usr/local/src/drupal.tar.gz drupal

    mkdir -p /opt/drupal/web/profiles/ingenieure
    cp -R /usr/local/src/ingenieure /opt/drupal/web/profiles

    echo "Restoring /opt/drupal/web/sites directory from /tmp"
    rm -rf /opt/drupal/web/sites
    mv /tmp/sites /opt/drupal/web/

    ln -sf /opt/drupal/web /var/www/html
    if [ "$EXTERNAL_DATABASE" == "1" ]; then
        echo "Use external database..."
    else 
        install_internal_db
    fi
    wait_for_db

    echo "Running database updates"
    cd /opt/drupal
    ls -la /opt/drupal/web/sites/default/civicrm.settings.php
    drush updatedb --uri="https://$DRUPAL_TRUSTED_HOST" --no-interaction
    cv upgrade:db

    install_civicrm_ext
    cv upgrade:db

    echo "Additional cleanup"
    cv flush
    rm -rf /opt/drupal/web/sites/default/files/civicrm/templates_c/
    composer civicrm:publish

    echo "Disable maintenance mode"
    drush maint:set 0
    touch /opt/drupal/finished
else
    ln -sf /opt/drupal/web /var/www/html
    if [ "$EXTERNAL_DATABASE" == "1" ]; then
        echo "Use external database..." 
    else
        service mariadb start
    fi
    wait_for_db
fi

chmod a-w /opt/drupal/web/sites/default/civicrm.settings.php
apache2-foreground